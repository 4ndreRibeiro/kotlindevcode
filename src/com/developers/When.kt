package com.developers

fun converteNovo(muito: Double, moeda: Moeda) = when(moeda){
    Moeda.USD -> muito * 3.4
    Moeda.BRA -> muito * 5.24
    Moeda.PEN -> muito
    Moeda.ARG -> muito * 4.7
    Moeda.MXN -> muito * 5.97
    Moeda.BOB -> muito * 2.04
    Moeda.CLP -> muito * 192.21
    Moeda.COP -> muito * 882.20
    Moeda.PYG -> muito * 1713.43
    Moeda.UYU -> muito * 8.40
}
fun obterRegiaoMoeda(moeda: Moeda) = when(moeda){
    Moeda.MXN,    Moeda.USD -> "NorteAmericana"
    Moeda.BRA,
    Moeda.PEN,
    Moeda.ARG,
    Moeda.MXN,
    Moeda.BOB,
    Moeda.CLP,
    Moeda.COP,
    Moeda.PYG,
    Moeda.UYU -> "Sudamerica"
}

fun main(args: Array<String>){
    println(converteNovo(1000.0, Moeda.BRA))
    println(obterRegiaoMoeda(Moeda.UYU))
    println(obterRegiaoMoeda(Moeda.USD))
}