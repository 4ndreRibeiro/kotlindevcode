package com.developers

import java.util.Random

class Pessoa (val nome: String, var idade: Int){
    val maiorIdade: Boolean
        get() = if (idade >= 18)true else false
}

fun criarPessoa(nome: String): Pessoa = Pessoa(nome, 1 + Random().nextInt(85))