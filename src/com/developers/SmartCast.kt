package com.developers

interface Expresion
class Numero(val valor: Int) : Expresion
class Sumar(val valorA: Expresion, val valorB: Expresion) : Expresion
class Multiplicar(val valorA: Expresion, val valorB: Expresion) : Expresion

fun evoluirExpresion(expresion: Expresion): Int = when(expresion){
    is Numero -> expresion.valor
    is Sumar -> evoluirExpresion(expresion.valorA) + evoluirExpresion(expresion.valorB)
    is Multiplicar -> evoluirExpresion(expresion.valorA) * evoluirExpresion(expresion.valorB)
    else ->
        throw IllegalArgumentException("No se puede reconecer la expresión")
    }

fun main(args: Array<String>){
    println(evoluirExpresion(Sumar(Sumar(Multiplicar(Multiplicar(Numero(3), Numero(4)), Numero(2)), Numero(2)), Numero(4))))
}