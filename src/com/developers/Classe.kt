package com.developers

fun main(args: Array<String>){
    val pessoa = Pessoa("André Ribeiro", 36)
    println(pessoa.nome)
    println(pessoa.idade)
    println(pessoa.maiorIdade)

    pessoa.idade = 17
    println(pessoa.idade)
    println(pessoa.maiorIdade)

}