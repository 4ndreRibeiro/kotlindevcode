package com.developers

enum class Moeda(val simbolo:String){
   BRA("R$"), USD("$"), PEN("S/."), ARG("$"), MXN("$"), BOB("\$b"), CLP("$"),COP("$"), PYG("Gs"), UYU("\$U");

    fun formato(muito: Double) = "$simbolo $muito"
}

fun main(args: Array<String>){
    println(Moeda.ARG)
    println(Moeda.BOB.simbolo)
    println(Moeda.PEN.formato(120.0))
    println(Moeda.BRA.formato(1.500))
}