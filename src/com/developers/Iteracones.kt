package com.developers

fun main (args: Array<String>){

    /*
    var i = 0
    while (i < 3){
        println(i)
        i++
    }
    do {
        println(i)
        i++
    }while (i < 6)
    */
    /*
    for (i in 10..20 step 2){
        println(i)
    }
     */
    val array = arrayListOf("A", "B", "C", "D", "E")

    for ((index, c) in array.withIndex()){
        println("$index $c")
    }

}