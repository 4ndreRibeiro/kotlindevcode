import java.util.Scanner

fun main(args: Array<String>){
    println("Escreva seu nome ou presione [enter] para seguir anomimo: ")
    val scanner = Scanner(System.`in`)
    val nextLine = scanner.nextLine()

    println("Hola ${if (nextLine != "") nextLine else "Kotlin"}")
}